# Lista de Tarefas

Esta app permite armazenar e reuperar informacoes sobre tarefas executadas ou a executar no dia pertinente.

## Descricao dos arquivos

* winmain.exe: a aplicacao em modo binario
* gerador: gerador de grafos
* grafos: grafos gerados
* screencastas: videos mostrando a execucao.
* src: codigo-fonte
* linhas.sh: conta quantas linhas de programacao a aplicaco possui.
* 20*.bin: arquivos onde estao ficam armazenadas as inforamacoes.
