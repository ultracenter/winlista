#!/bin/bash

CC="mingw32-gcc"
LDFLAGS="-s -lcomctl32 -lcomdlg32 -Wl,--subsystem,windows"
CFLAGS="-fdump-rtl-expand -O3 -std=c11 -D WINDOWS -D _WIN32_IE=0x0500 -D WINVER=0x500"

${CC} ${CFLAGS} *.c

