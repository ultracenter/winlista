# This Makefile will build the MinGW Win32 application.

#HEADERS = include/callbacks.h include/resource.h
OBJS =	obj/WinMain.o obj/WndProc.o obj/Lista.o obj/Trace.o obj/asprintf.o obj/Dir.o
INCLUDE_DIRS = -I./include /usr/local/mingw32/include


WARNS = -Wall

#CC = i686-w64-mingw32-gcc
#CC = i686-w64-mingw32-gcc-win32
CC = mingw32-gcc
LDFLAGS = -s -lcomctl32 -lcomdlg32 -Wl,--subsystem,windows 
#RC = i686-w64-mingw32-windres
RC = mingw32-windres

CFLAGS= -fdump-rtl-expand -O3 -std=c11 -D WINDOWS -D _WIN32_IE=0x0500 -D WINVER=0x500 ${WARNS}


all: winmain.exe

winmain.exe: ${OBJS}
	${CC} ${CFLAGS} -o "$@" ${OBJS} ${LDFLAGS}

clean:
	rm -f obj/*.o "winmain.exe"

obj/%.o: src/%.c
#${HEADERS}
	${CC} ${CFLAGS} ${INCLUDE_DIRS} -c $< -o $@

obj/resource.o: res/resource.rc res/Application.manifest res/Application.ico res/resource.h
	${RC} -I.\include -I.\res -i $< -o $@


